﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using AspNetWebApiRest.Models;

// https://developer.okta.com/blog/2019/03/13/build-rest-api-with-aspnet-web-api

namespace AspNetWebApiRest.Controllers
{
    public class ListItemsController : ApiController
    {
        // https://stackoverflow.com/a/9782851
        private static List<CustomListItem> _listItems { get; set; } = new List<CustomListItem>();
        //private XmlDocument _xmlDoc { get; set; } = new XmlDocument();

        // GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}
        public IEnumerable<CustomListItem> Get()
        {
            return _listItems;
        }

        // GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}
        public HttpResponseMessage Get(int id)
        {
            var item = _listItems.FirstOrDefault(x => x.Id == id);
            if (item != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, item);
            }
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        // POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}
        public HttpResponseMessage Post([FromBody]CustomListItem model)
        {
            if (string.IsNullOrEmpty(model?.Text))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            
            var maxId = 0;
            
            if (_listItems.Count > 0)
            {
                maxId = _listItems.Max(x => x.Id);
            }
            
            model.Id = maxId + 1;
            //string testing1 = RetrieveMCNewNetworkXML();
            //model.Mxxml = testing1;
            model.Mcxmlurl = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            //model.Mxxml = "";
            model.Mcxmlouter = model.RetrieveMCNewsNetworkOuterXML();
            model.Mcxmlinner = model.RetrieveMCNewsNetworkInnerText();
            model.Mxxml = model.RetrieveMCNewNetworkXML();
            model.Mcxmlnodaily = model.RetrieveMCNewNetworkXMLNoDaily();
            model.Mcappcontenturl = "https://content.mcmapp.mayoclinic.org/api/v2/content/list";
            //model.Mcappcontentjson = "";
            model.Mcappcontentjson = model.RetrieveMCAppContentJSON();
            //model.Mcnewsitemstitles = model.RetrieveMCNewsNetworkItemsTitle().Count + "";
            model.Mcnewsitemstitles = model.RetrieveMCNewsNetworkItemsTitle();
            model.Mcnewsitemslinks = model.RetrieveMCNewsNetworkItemsLinks();
            model.Mcnewsitemsdescription = model.RetrieveMCNewsNetworkItemsDescriptions();
            model.Mcnewsitemsguids = model.RetrieveMCNewsNetworkItemsGuids();
            model.Mcnewspubdates = model.RetrieveMCNewsNetworkItemsPubDates();
            model.Mcnewsitemscats = model.RetrieveMCNewsNetworkItemsCats();
            model.Mcnewsitemscreators = model.RetrieveMCNewsNetworkItemsCreators();
            model.Mcnewsdailybadcats = model.RetrieveMCNewsNetworkDailyBadCats();
            model.Mcnewsallcats = model.RetrieveMCNewsNetworkItemsAllCats();
            model.Todaysdate = model.GetDate();
            model.Mccontentitems = model.RetrieveMCContentAppItems();
            model.Deserializejsontest = model.DeserializeJSON();
            //MCContentAppSup macapp;
            //model.Supplementjson = macapp;
            model.SupJSON = model.MakeMCContentAppObj(model);
            //model.Supplementjson = model.SetupSupplementJSON();

            // NOTE TO SELF: ROTATE THROUGH CATS TO BASED ON CATS LIST TO KNOW WHEN TO STOP AND GET THE DAILY

            _listItems.Add(model);
            return Request.CreateResponse(HttpStatusCode.Created, model);
        }

        // PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}
        public HttpResponseMessage Put(int id, [FromBody]CustomListItem model)
        {
            // ?. tests for null before performing member access
            // https://stackoverflow.com/a/39504036
            if (string.IsNullOrEmpty(model?.Text))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            var item = _listItems.FirstOrDefault(x => x.Id == id);
            if (item != null)
            {
                // Update *all* of the item's properties
                item.Text = model.Text;

                return Request.CreateResponse(HttpStatusCode.OK, item);
            }
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        // DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
        public HttpResponseMessage Delete(int id)
        {
            var item = _listItems.FirstOrDefault(x => x.Id == id);
            if (item != null)
            {
                _listItems.Remove(item);
                return Request.CreateResponse(HttpStatusCode.OK, item);
            }
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        // https://stackoverflow.com/questions/25783683/reading-xml-from-rest-api
        // https://support.microsoft.com/en-us/help/307643/how-to-read-xml-data-from-a-url-by-using-visual-c
        
        /*public string RetrieveMCNewNetworkXML()
        {
            string mcNewsURL = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            XmlTextReader reader = new XmlTextReader(mcNewsURL);
            //string xmlResults = "";
            string xmlResults = reader.ToString();

            //while (reader.Read())
            //{
            //    switch (reader.NodeType)
            //    {
            //        case XmlNodeType.Element: // The node is an element.
            //            Console.Write("<" + reader.Name);
            //            xmlResults = xmlResults + "<" + reader.Name;

            //            while (reader.MoveToNextAttribute()) // Read the attributes.
            //                Console.Write(" " + reader.Name + "='" + reader.Value + "'");
                        
            //            xmlResults = xmlResults + " " + reader.Name + "='" + reader.Value + "'";
            //            Console.Write(">");
            //            xmlResults += ">";
            //            Console.WriteLine(">");
            //            xmlResults += ">";
            //            break;

            //        case XmlNodeType.Text: //Display the text in each element.
            //            Console.WriteLine(reader.Value);
            //            xmlResults += reader.Value;
            //            break;

            //        case XmlNodeType.EndElement: //Display the end of the element.
            //            Console.Write("</" + reader.Name);
            //            xmlResults = xmlResults + "</" + reader.Name;
            //            Console.WriteLine(">");
            //            xmlResults += ">";
            //            break;
            //    }
            //}

            return xmlResults;

            //string response;
            //try
            //{
            //    using (StreamReader streamIn = new StreamReader((webRequest.GetResponse()).GetResponseStream()))
            //    {
            //        response = streamIn.ReadToEnd();
            //        streamIn.Close();
            //    }
            //}
            //finally
            //{ webRequest.Abort(); }

            //XDocument xDoc = XDocument.Parse(response);
        }
        */
    }
}