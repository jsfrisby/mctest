﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;

namespace AspNetWebApiRest.Models
{
    public class CustomListItem
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Mcxmlurl { get; set; }
        //public string Mcxmlurl = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
        public string Mcxmlouter { get; set; }
        public string Mcxmlinner { get; set; }
        public string Mxxml { get; set; }
        public string Mcxmlnodaily { get; set; }
        public string Mcappcontenturl { get; set; }
        public string Mcappcontentjson { get; set; }
        public List<string> Mcnewsitemstitles { get; set; }
        //public string Mcnewsitemstitles { get; set; }
        public List<string> Mcnewsitemscreators { get; set; }
        public List<string> Mcnewsitemscats { get; set; }
        public List<string> Mcnewsitemslinks { get; set; }
        public List<string> Mcnewsitemsguids { get; set; }
        public List<string> Mcnewsitemsdescription { get; set; }
        public List<string> Mcnewsallcats { get; set; }
        public List<bool> Mcnewsdailybadcats { get; set; }
        public List<string> Mcnewspubdates { get; set; }
        public string Todaysdate { get; set; }
        public List<string> Mccontentitems { get; set; }
        public MCContentAppSup Deserializejsontest { get; set; }
        public MCContentAppSup SupJSON { get; set; }
        public string Supplementjson { get; set; }


        // get the outerXML from the MC News Network RSS feed
        public string RetrieveMCNewsNetworkOuterXML()
        {
            string mcNewsURL = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            XmlTextReader reader = new XmlTextReader(mcNewsURL);
            string xmlResults = "";

            XmlDocument doc1 = new XmlDocument();
            doc1.Load(mcNewsURL);
            XmlNode root = doc1.DocumentElement;
            XmlNodeList nodes = root.SelectNodes("/");

            foreach (XmlNode node in nodes)
            {
                xmlResults += node.OuterXml;
            }

            return xmlResults;
        }

        // get the inner text from the MC News Network RSS feed
        public string RetrieveMCNewsNetworkInnerText()
        {
            string mcNewsURL = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            XmlTextReader reader = new XmlTextReader(mcNewsURL);
            string xmlResults = "";

            XmlDocument doc1 = new XmlDocument();
            doc1.Load(mcNewsURL);
            XmlNode root = doc1.DocumentElement;
            XmlNodeList nodes = root.SelectNodes("/");

            foreach (XmlNode node in nodes)
            {
                xmlResults += node.OuterXml;
            }

            return xmlResults;
        }

        // get the items titles text from the MC News Network RSS feed
        public List<string> RetrieveMCNewsNetworkItemsTitle()
        {
            string mcNewsURL = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            XmlTextReader reader = new XmlTextReader(mcNewsURL);
            //string xmlResults = "";
            List<string> resultsList = new List<string>();

            XmlDocument doc1 = new XmlDocument();
            doc1.Load(mcNewsURL);
            XmlNode root = doc1.DocumentElement;
            XmlNodeList nodes = root.SelectNodes("//channel/item");

            foreach (XmlNode node in nodes)
            {
                //xmlResults += node["title"].InnerText;
                //xmlResults += node["title"].OuterXml;
                resultsList.Add(node["title"].InnerText);
            }

            return resultsList;
        }

        // get the items links from the MC News Network RSS feed
        public List<string> RetrieveMCNewsNetworkItemsLinks()
        {
            string mcNewsURL = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            XmlTextReader reader = new XmlTextReader(mcNewsURL);
            //string xmlResults = "";
            List<string> resultsList = new List<string>();

            XmlDocument doc1 = new XmlDocument();
            doc1.Load(mcNewsURL);
            XmlNode root = doc1.DocumentElement;
            XmlNodeList nodes = root.SelectNodes("//channel/item");

            foreach (XmlNode node in nodes)
            {
                //xmlResults += node["title"].InnerText;
                //xmlResults += node["title"].OuterXml;
                resultsList.Add(node["link"].InnerText);
            }

            return resultsList;
        }

        // get the items descriptions from the MC News Network RSS feed
        public List<string> RetrieveMCNewsNetworkItemsDescriptions()
        {
            string mcNewsURL = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            XmlTextReader reader = new XmlTextReader(mcNewsURL);
            List<string> resultsList = new List<string>();

            XmlDocument doc1 = new XmlDocument();
            doc1.Load(mcNewsURL);
            XmlNode root = doc1.DocumentElement;
            XmlNodeList nodes = root.SelectNodes("//channel/item");

            foreach (XmlNode node in nodes)
            {
                resultsList.Add(node["description"].InnerText);
            }

            return resultsList;
        }

        // get the items guids from the MC News Network RSS feed
        public List<string> RetrieveMCNewsNetworkItemsGuids()
        {
            string mcNewsURL = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            XmlTextReader reader = new XmlTextReader(mcNewsURL);
            List<string> resultsList = new List<string>();

            XmlDocument doc1 = new XmlDocument();
            doc1.Load(mcNewsURL);
            XmlNode root = doc1.DocumentElement;
            XmlNodeList nodes = root.SelectNodes("//channel/item");

            foreach (XmlNode node in nodes)
            {
                resultsList.Add(node["guid"].InnerText);
            }

            return resultsList;
        }

        // get the items pub dates from the MC News Network RSS feed
        public List<string> RetrieveMCNewsNetworkItemsPubDates()
        {
            string mcNewsURL = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            XmlTextReader reader = new XmlTextReader(mcNewsURL);
            List<string> resultsList = new List<string>();

            XmlDocument doc1 = new XmlDocument();
            doc1.Load(mcNewsURL);
            XmlNode root = doc1.DocumentElement;
            XmlNodeList nodes = root.SelectNodes("//channel/item");

            foreach (XmlNode node in nodes)
            {
                resultsList.Add(node["pubDate"].InnerText);
            }

            return resultsList;
        }

        // get the items categories from the MC News Network RSS feed
        public List<string> RetrieveMCNewsNetworkItemsCats()
        {
            string mcNewsURL = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            XmlTextReader reader = new XmlTextReader(mcNewsURL);
            List<string> resultsList = new List<string>();

            XmlDocument doc1 = new XmlDocument();
            doc1.Load(mcNewsURL);
            XmlNode root = doc1.DocumentElement;
            XmlNodeList nodes = root.SelectNodes("//channel/item");

            foreach (XmlNode node in nodes)
            {
                resultsList.Add(node["category"].InnerText);
            }

            return resultsList;
        }

        // get the items creators from the MC News Network RSS feed
        public List<string> RetrieveMCNewsNetworkItemsCreators()
        {
            string mcNewsURL = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            XmlTextReader reader = new XmlTextReader(mcNewsURL);
            List<string> resultsList = new List<string>();

            XmlDocument doc1 = new XmlDocument();
            doc1.Load(mcNewsURL);
            XmlNode root = doc1.DocumentElement;
            XmlNodeList nodes = root.SelectNodes("//channel/item");

            foreach (XmlNode node in nodes)
            {
                resultsList.Add(node["dc:creator"].InnerText);
            }

            return resultsList;
        }

        // get the items categories and mark daily cats as true from the MC News Network RSS feed to filter them out later
        public List<bool> RetrieveMCNewsNetworkDailyBadCats()
        {
            string mcNewsURL = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            XmlTextReader reader = new XmlTextReader(mcNewsURL);
            List<bool> resultsList = new List<bool>();

            XmlDocument doc1 = new XmlDocument();
            doc1.Load(mcNewsURL);
            XmlNode root = doc1.DocumentElement;
            XmlNodeList nodes = root.SelectNodes("//channel/item");
            string inText = "";

            List<string> allCats = new List<string>();
            List<string> firstCats = new List<string>();

            allCats = RetrieveMCNewsNetworkItemsAllCats();
            firstCats = RetrieveMCNewsNetworkItemsCats();

            int count = 0;
            bool firstIteration = true;
            bool daily = false;     // was daily found in this grouping of cats
            string firstCat = firstCats.First();
            bool loopToNextRound = false;

            foreach (string cat in allCats)
            {
                if (firstCat.Equals(cat)) // && !firstCat.Equals("daily"))
                {                    
                    loopToNextRound = false;

                    //if (!firstIteration)
                    //    count++;

                    if (!firstIteration && !daily)
                    {
                        resultsList.Add(false);
                        count++;
                    }

                    //firstIteration = false;
                    daily = false;  // start fresh

                    if (count < firstCats.Count - 1)
                        firstCat = firstCats.ElementAt(count + 1);

                    continue;
                }
                else if (loopToNextRound)
                {
                    continue;
                }

                if (cat.Contains("daily"))
                {
                    resultsList.Add(true);
                    loopToNextRound = true;
                    daily = true;

                    count++;
                    //firstCat = firstCats.ElementAt(count + 1);
                    //firstIteration = false;
                }

                firstIteration = false;
            }

            return resultsList;
        }

        // get the items all categories from the MC News Network RSS feed
        public List<string> RetrieveMCNewsNetworkItemsAllCats()
        {
            string mcNewsURL = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            XmlTextReader reader = new XmlTextReader(mcNewsURL);
            List<string> resultsList = new List<string>();

            XmlDocument doc1 = new XmlDocument();
            doc1.Load(mcNewsURL);
            XmlNode root = doc1.DocumentElement;
            XmlNodeList nodes = root.SelectNodes("//channel/item/category");

            foreach (XmlNode node in nodes)
            {
                resultsList.Add(node.InnerText);
            }

            return resultsList;
        }

        // https://stackoverflow.com/questions/25783683/reading-xml-from-rest-api
        // https://support.microsoft.com/en-us/help/307643/how-to-read-xml-data-from-a-url-by-using-visual-c
        public string RetrieveMCNewNetworkXML()
        {
            string mcNewsURL = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            XmlTextReader reader = new XmlTextReader(mcNewsURL);
            string xmlResults = "";
            //string xmlResults = reader.ToString();

            //while (reader.Read())
            //{
            //    switch (reader.NodeType)
            //    {
            //        case XmlNodeType.Element: // The node is an element.
            //            //Console.Write("<" + reader.Name);
            //            xmlResults = xmlResults + "<" + reader.Name;

            //            while (reader.MoveToNextAttribute()) // Read the attributes.
            //            {
            //                //Console.Write(" " + reader.Name + "='" + reader.Value + "'");
            //                xmlResults = xmlResults + " " + reader.Name + "='" + reader.Value + "'";
            //            }

            //            //Console.Write(">");
            //            xmlResults += ">";
            //            //Console.WriteLine(">");
            //            xmlResults += ">";
            //            break;

            //        case XmlNodeType.Text: //Display the text in each element.
            //            //Console.WriteLine(reader.Value);
            //            xmlResults += reader.Value;
            //            break;

            //        case XmlNodeType.EndElement: //Display the end of the element.
            //            //Console.Write("</" + reader.Name);
            //            xmlResults = xmlResults + "</" + reader.Name;
            //            //Console.WriteLine(">");
            //            xmlResults += ">";
            //            break;
            //    }
            //}

            xmlResults = "";

            XmlDocument doc1 = new XmlDocument();
            doc1.Load(mcNewsURL);
            //xmlResults = doc1.Value;
            //XmlElement root = doc1.DocumentElement;
            XmlNode root = doc1.DocumentElement;
            //XmlNodeList nodes = root.SelectNodes("/");
            //XmlNodeList nodes = root.SelectNodes("descendant::item:item[item:category!='daily']");
            XmlNodeList nodes = root.SelectNodes("//channel");

            // https://stackoverflow.com/a/14927965
            foreach (XmlNode node in nodes)
            {
                //xmlResults += "OUTER1";
                //xmlResults += node.OuterXml;
                //xmlResults += "INNER1";
                //xmlResults += node.InnerText;
                //xmlResults += "OUTER2";
                //xmlResults += node.OuterXml;
                //xmlResults += node["channel"].InnerText;
                xmlResults += node["title"].InnerText;
                xmlResults += node["description"].InnerText;
                //xmlResults += node["category"].InnerText;
                //xmlResults += node["item:title"].InnerText;
            }

            XmlNodeList nodes2 = root.SelectNodes("//channel/item");

            int counter = 0;
            int innerCounter = 0;
            int maxInnerCount = 0;

            //for (int i = 0; i < nodes2.ChildNodes.Count; i++)
            //{
            //    xmlResults = xmlResults + "i: " + i;
            //    xmlResults += nodes2.ChildNodes[i].OuterXml;
            //}

            //XmlNode testNode = doc1.FirstChild;

            ////Display the contents of the child nodes.
            //if (testNode.HasChildNodes)
            //{
            //    for (int i = 0; i < testNode.ChildNodes.Count; i++)
            //    {
            //        xmlResults += testNode.ChildNodes[i].OuterXml;
            //    }
            //}

            foreach (XmlNode node in nodes2)
            {
                XmlNodeList catNodes = node.SelectNodes("//category");

                xmlResults += "Counter: " + counter;

                foreach (XmlNode tempNode in catNodes)
                {
                    if (innerCounter < maxInnerCount)
                    {
                        innerCounter++;
                        continue;
                    }

                    xmlResults += tempNode.OuterXml;

                    if (tempNode.NextSibling.OuterXml.Contains("Featured News") && innerCounter == maxInnerCount)
                    {
                        maxInnerCount += innerCounter;
                        break;
                    }

                    xmlResults += "Next: " + counter;
                    xmlResults += tempNode.NextSibling.OuterXml;
                    //xmlResults += tempNode.InnerText;

                    //tempNode = tempNode.NextSibling;
                }

                innerCounter = 0;
                counter++;

                //xmlResults += node["category"].OuterXml;
                //xmlResults += node["category"].
                //xmlResults += node["category:1"].OuterXml;
                //xmlResults += node["category:2"].OuterXml;
                //xmlResults += node["category[3]"].OuterXml;
                //xmlResults += node["category[4]"].OuterXml;
                //xmlResults += node["category[5]"].OuterXml;
                //xmlResults += node["category"].InnerText;
            }

            //XmlNodeList nodes3 = root.SelectNodes("//channel/item/category");

            //foreach (XmlNode node in nodes3)
            //{
            //    xmlResults += node.OuterXml;
            //    xmlResults += node.InnerText;
            //}

            return xmlResults;
        }

        // https://docs.microsoft.com/en-us/dotnet/standard/data/xml/select-nodes-using-xpath-navigation
        public string RetrieveMCNewNetworkXMLNoDaily()
        {
            string mcNewsURL = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            XmlTextReader reader = new XmlTextReader(mcNewsURL);
            string xmlResults = "";

            XmlDocument doc1 = new XmlDocument();
            doc1.Load(mcNewsURL);
            //xmlResults = doc1.Value;
            XmlElement root = doc1.DocumentElement;
            XmlNodeList nodes = root.SelectNodes("/");
            List<string> items = new List<string>();
            bool dailyEncountered = false;

            foreach (XmlNode node in nodes)
            {
                Console.WriteLine(node.ToString());
                if (node.OuterXml == "<item>")
                    items.Clear();
                else if (node.OuterXml == "</item>")
                {
                    if (!dailyEncountered)
                        foreach (string i in items)
                        {
                            xmlResults += i;
                        }
                        //xmlResults += items.ToString();

                    items.Clear();
                    dailyEncountered = false;
                }
                else
                {
                    if (node.InnerText == "daily")
                        dailyEncountered = true;

                    items.Add(node.OuterXml);
                    items.Add(node.InnerText);
                    items.Add(node.OuterXml);
                }

                
                //xmlResults += node.OuterXml;
                //xmlResults += node.InnerText;
                //xmlResults += node.OuterXml;

                
            }

            return xmlResults;
        }

        // https://stackoverflow.com/questions/46271214/get-json-post-data-in-c-sharp
        public string RetrieveMCAppContentJSON()
        {
            var jsonResults = "";
            string mcAppContentUrl = "https://content.mcmapp.mayoclinic.org/api/v2/content/list";

            //using (var w = new WebClient())
            //{
            //    var json_data = string.Empty;
            //    // attempt to download JSON data as a string
            //    try
            //    {
            //        json_data = w.DownloadString(mcAppContentUrl);
            //    }
            //    catch (Exception) { }
            //    // if string with JSON data is not empty, deserialize it to class and return its instance 
            //    //return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json_data) : new T();
            //    jsonResults = (string) JsonConvert.DeserializeObject(json_data);
            //    return jsonResults;
            //}

            // https://stackoverflow.com/a/44676959
            //string myJson = "{'Username': 'myusername','Password':'pass'}";
            //string jsonObj = "{'ApplicationId':'MyMayoClinic', 'CategoryId':'','DocumentId':'', 'RequestDate':'2020-03-02', 'SessionId':'{{sessionId}}','ApplicationVersion':'6.4'}'";
            //string jsonObj = "{\"ApplicationId\":\"MyMayoClinic\", \"CategoryId\":\"\",\"DocumentId\":\"\", \"RequestDate\":\"2020-03-02\", \"SessionId\":\"{{sessionId}}\",\"ApplicationVersio\":\"6.4\"}\"";

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(mcAppContentUrl);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            using (var streamWriter = new

            StreamWriter(httpWebRequest.GetRequestStream()))
            {
                //string json = new JavaScriptSerializer().Serialize(new
                //{
                //    ApplicationId = "MyMayoClinic",
                // CategoryId = "",
                //    DocumentId = "",
                // RequestDate = "2020-03-02",
                // SessionId = "{{sessionId}}",
                // ApplicationVersion = "6.4"
                //});

                string json = new JavaScriptSerializer().Serialize(new
                {
                    ApplicationId = "MyMayoClinic",
                    CategoryId = "",
                    DocumentId = "",
                    RequestDate = GetDate(),
                    SessionId = "{{sessionId}}",
                    ApplicationVersion = "6.4"
                });

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                jsonResults = streamReader.ReadToEnd();
            }


            // almost worked
            //using (var client = new HttpClient())
            //{
            //    //var response = await client.PostAsync(
            //    var response = client.PostAsync(
            //        mcAppContentUrl,
            //         new StringContent(jsonObj, Encoding.UTF8, "application/json"));

            //    //jsonResults = response.ToString();
            //    jsonResults = response.Result + "";
            //}


            //var http = (HttpWebRequest)WebRequest.Create(new Uri(mcAppContentUrl));
            //http.Accept = "application/json";
            //http.ContentType = "application/json";
            //http.Method = "POST";

            //// https://stackoverflow.com/a/15091622
            //string parsedContent = << PUT HERE YOUR JSON PARSED CONTENT >>;
            //ASCIIEncoding encoding = new ASCIIEncoding();
            //Byte[] bytes = encoding.GetBytes(parsedContent);

            //Stream newStream = http.GetRequestStream();
            //newStream.Write(bytes, 0, bytes.Length);
            //newStream.Close();

            //var response = http.GetResponse();

            //var stream = response.GetResponseStream();
            //var sr = new StreamReader(stream);
            //var content = sr.ReadToEnd();

            return jsonResults;
        }

        // https://www.techiedelight.com/get-current-date-without-time-csharp/
        public string GetDate()
        {
            return (string) DateTime.UtcNow.ToString("yyyy-MM-dd");
        }

        public List<string> RetrieveMCContentAppItems()
        {
            // https://stackoverflow.com/questions/46485853/how-to-insert-c-sharp-string-into-json-string

            //string mcNewsURL = "https://newsnetwork.mayoclinic.org/tag/newsapp/feed/";
            //XmlTextReader reader = new XmlTextReader(mcNewsURL);
            List<string> resultsList = new List<string>();

            //XmlDocument doc1 = new XmlDocument();
            //doc1.Load(mcNewsURL);
            //XmlNode root = doc1.DocumentElement;
            //XmlNodeList nodes = root.SelectNodes("//channel/item");

            //foreach (XmlNode node in nodes)
            //{
            //    resultsList.Add(node["dc:creator"].InnerText);
            //}

            return resultsList;
        }

        public MCContentAppSup DeserializeJSON()
        {
            MCContentAppSup deserializeObj = JsonConvert.DeserializeObject<MCContentAppSup>(RetrieveMCAppContentJSON());

            return deserializeObj;
        }

        public MCContentAppSup MakeMCContentAppObj()
        {
            MCContentAppSup macapp = new MCContentAppSup();
            Item i = macapp.MakeNewItem();
            //macapp.Items = new List<Item>();
            macapp.Items.Add(i);

            return macapp;
        }

        public MCContentAppSup MakeMCContentAppObj(CustomListItem cliObj)
        {
            MCContentAppSup macapp = new MCContentAppSup();
            //Item i = macapp.MakeNewItem();
            //macapp.Items = new List<Item>();

            int len = 0;

            foreach (bool b in cliObj.Mcnewsdailybadcats)
            {
                if (!b)
                {
                    Item i = macapp.MakeNewItem();

                    i.Creator = cliObj.Mcnewsitemscreators[len];
                    i.Link = cliObj.Mcnewsitemslinks[len];
                    i.Title = cliObj.Mcnewsitemstitles[len];
                    i.Guid = cliObj.Mcnewsitemsguids[len];
                    i.LongDescription = cliObj.Mcnewsitemsdescription[len];
                    i.PubDate = cliObj.Mcnewspubdates[len];

                    // extra categories requires a List<string> and the List needs to be populated from the range in allcats that has the non daily cats
                    i.ExtraCategories = new List<string>();

                    macapp.Items.Add(i);
                }

                len++;  // must len++ outside of the if statement to get the proper index
            }



            //i.Guid = cliObj.Mcnewsitemsguids[0];
            //macapp.Items.Add(i);

            return macapp;
        }

        public string SetupSupplementJSON()
        {
            // https://stackoverflow.com/questions/6620165/how-can-i-parse-json-with-c
            string supJSON = "";

            return supJSON; // What's up Jason?
        }
    }
}