﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspNetWebApiRest.Models
{
    public class Authors
    {
        public string Name { get; set; }
        public string Photo { get; set; }
    }
}