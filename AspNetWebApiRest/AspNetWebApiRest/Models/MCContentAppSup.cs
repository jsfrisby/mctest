﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspNetWebApiRest.Models
{
    public class MCContentAppSup
    {
        public string Id { get; set; }
        // https://stackoverflow.com/questions/46485853/how-to-insert-c-sharp-string-into-json-string
        //public Item Items { get; set; }
        [JsonProperty("Items")]
        public List<Item> Items { get; set; } = new List<Item>();
        //public List<Item> Items { get; set; }
        //public Dictionary<string, string> Items { get; set; }

        public Item MakeNewItem()
        {
            Item i = new Item();
            i.Author = i.MakeNewAuthor();
            i.Category = i.MakeNewCategory();
            i.ExtraCategories = i.MakeNewExtraCategories();
            i.Images = i.MakeNewImageList();

            return i;
        }
    }
}