﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspNetWebApiRest.Models
{
    public class Item
    {
        public List<Image> Images { get; set; } = new List<Image>();
        public bool IsScheduled { get; set; }
        public Authors Author { get; set; }
        public int ReadTime { get; set; }
        public string Id { get; set; }
        public Categories Category { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Version { get; set; }
        public string Creator { get; set; }
        public string Link { get; set; }
        public string Title { get; set; }
        public string Guid { get; set; }
        public string LongDescription { get; set; }
        public string PubDate { get; set; }
        public List<string> ExtraCategories { get; set; }

        public Authors MakeNewAuthor()
        {
            Authors author = new Authors();

            return author;
        }

        public Categories MakeNewCategory()
        {
            Categories cat = new Categories();

            return cat;
        }

        public List<string> MakeNewExtraCategories()
        {
            List<string> xCat = new List<string>();

            return xCat;
        }

        public List<Image> MakeNewImageList()
        {
            List<Image> imageList = new List<Image>();

            return imageList;
        }
    }
}